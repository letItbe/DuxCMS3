<?php

/**
 * 系统首页
 */

namespace app\index\merchant;

class IndexMerchant extends \app\merchant\merchant\Merchant {

    /**
     * 首页
     */
    public function index() {
        $this->redirect(url('merchant/Index/index'));
    }

}